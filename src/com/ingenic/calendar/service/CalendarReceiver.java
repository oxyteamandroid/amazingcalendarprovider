/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendarProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.service;

import com.ingenic.iwds.utils.IwdsLog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class CalendarReceiver extends BroadcastReceiver {

    static final String TAG = CalendarReceiver.class.getSimpleName();
    private Intent mIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        mIntent = new Intent(context, SyncCalendarService.class);
        context.startService(mIntent);
        IwdsLog.d(TAG, "Calendar service is started");
    }
}
