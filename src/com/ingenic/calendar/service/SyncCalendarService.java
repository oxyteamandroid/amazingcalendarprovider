/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendarProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.calendar.service;

import android.app.Notification;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.IBinder;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.ScheduleInfo;
import com.ingenic.iwds.datatransactor.elf.ScheduleTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.provider.calendar.CalendarContrat;
import com.ingenic.provider.calendar.CalendarDatabaseHelper;

public class SyncCalendarService extends Service {

    private static final String TAG = "SyncCalendarService";

    /**
     * 连接手机时用到的UUID
     */
    public static final String UUID = "3b11571f-64eb-4085-1e5e-50fb6f0aff83";

    /**
     * 更新日历的广播ACTION
     */
    public static final String UPDATE_CALENDAR = "com.ingenic.action.UPDATE_CALENDAR";

    /**
     * 更新日程的广播ACTION
     */
    public static final String UPDATE_SCHEDULE = "com.ingenic.action.UPDATE_SCHEDULE";

    /**
     * 日程数据事物模型
     */
    private static ScheduleTransactionModel mTransactionModel;

    private CalendarDatabaseHelper mDBHelper;

    private Intent mUpdateCalendarIntent;
    private Intent mUpdateScheduleIntent;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // 使得服务不得被杀死
        Notification notification = new Notification();
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;
        startForeground(9999, notification);

        if (mTransactionModel == null) {
            mTransactionModel = new ScheduleTransactionModel(this,
                    mScheduleCallBack, UUID);
        }

        IwdsLog.d(TAG, "Calendar - TransactionModel is started!");
        mTransactionModel.start();

        if (mDBHelper == null) {
            mDBHelper = CalendarDatabaseHelper.getIntance(this);
        }

        mUpdateCalendarIntent = new Intent(UPDATE_SCHEDULE);
        mUpdateScheduleIntent = new Intent(UPDATE_CALENDAR);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTransactionModel != null) {
            mTransactionModel.stop();
        }
    }

    /**
     * 日程同步的传输回调
     */
    private ScheduleTransactionModel.ScheduleTransactionModelCallback mScheduleCallBack = new ScheduleTransactionModel.ScheduleTransactionModelCallback() {

        @Override
        public void onRequest() {
            IwdsLog.d(TAG, "Calendar - On Request");
        }

        @Override
        public void onRequestFailed() {
            IwdsLog.e(TAG, "Calendar - On Request Failed");
        }

        @Override
        public void onObjectArrived(ScheduleInfo object) {
            IwdsLog.i(TAG, "Calendar - On Object Arrived " + object.toString());

            if (object.eventCount > 0) {
                // 保存数据到本地数据库
                insertScheduleToDateBase(object);

                // 通知日历更新界面
                sendBroadcast(mUpdateCalendarIntent);
            }
        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor,
                boolean isConnected) {
            IwdsLog.d(TAG, "Calendar - On Link Connected " + isConnected);
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            IwdsLog.d(TAG, "Calendar - On Channel Available " + isAvailable);

            if (isAvailable) {
                if (mTransactionModel != null) {
                    // 连接后需要主动请求日程信息
                    mTransactionModel.request();
                }
            } else {
                // 断开连接清除数据
                int result = removeSchedule();
                if(result != -1){
                    IwdsLog.d(TAG, "Remove all event successful!");
                    // 清除完数据后更新日历
                    sendBroadcast(mUpdateScheduleIntent);
                }
            }
        }

        @Override
        public void onSendResult(DataTransactResult result) {
            IwdsLog.d(TAG,
                    "Calendar - On Send Result " + result.getResultCode());
        }

        private ContentValues mValues = new ContentValues();

        /**
         * 插入一列日程数据数据库
         * 
         * @param info
         *            日程信息列表
         */
        private void insertScheduleToDateBase(ScheduleInfo info) {
            if ((mDBHelper == null) && (info == null)) {
                return;
            }

            int len = info.event.length;
            for (int i = 0; i < len; i++) {
                ScheduleInfo.Event event = info.event[i];
                if (event.dtStart < 0 || event.dtEnd < 0) {
                    // 将需要删除的消息删除掉
                    mDBHelper.calendarDelete(
                            CalendarDatabaseHelper.TABLE_REMINDERS,
                            CalendarContrat.Events._ID + " = ? ",
                            new String[] { String.valueOf(event.id) });
                    mDBHelper.calendarDelete(
                            CalendarDatabaseHelper.TABLE_EVENTS,
                            CalendarContrat.Events._ID + " = ? ",
                            new String[] { String.valueOf(event.id) });
                } else {
                    mValues.clear();
                    mValues.put(CalendarContrat.Events._ID, event.id);
                    mValues.put(CalendarContrat.Events.TITLE, event.title);
                    mValues.put(CalendarContrat.Events.EVENT_LOCATION,
                            event.eventLocation);
                    mValues.put(CalendarContrat.Events.DESCRIPTION,
                            event.description);
                    mValues.put(CalendarContrat.Events.DT_START, event.dtStart);
                    mValues.put(CalendarContrat.Events.DT_END, event.dtEnd);
                    mValues.put(CalendarContrat.Events.EVENT_TIMEZONE,
                            event.eventTimezone);
                    mValues.put(CalendarContrat.Events.DURATION, event.duration);
                    mValues.put(CalendarContrat.Events.ALL_DAY, event.allDay);
                    mValues.put(CalendarContrat.Events.HAS_ALARM,
                            event.hasAlarm);
                    mValues.put(CalendarContrat.Events.RRULE, event.rrule);
                    mDBHelper.calendarInsert(
                            CalendarDatabaseHelper.TABLE_EVENTS, mValues);

                    if (event.reminder != null) {
                        ScheduleInfo.Reminder reminder = event.reminder;
                        mValues.clear();
                        mValues.put(CalendarContrat.Reminders._ID, reminder.id);
                        mValues.put(CalendarContrat.Reminders.EVENT_ID,
                                event.id);
                        mValues.put(CalendarContrat.Reminders.MINUTES,
                                reminder.minutes);
                        mValues.put(CalendarContrat.Reminders.METHOD,
                                reminder.method);
                        mDBHelper
                                .calendarInsert(
                                        CalendarDatabaseHelper.TABLE_REMINDERS,
                                        mValues);
                    }
                }
            }
        }

        /**
         * 删除日程信息
         * 
         * @param where
         *            过滤条件
         * @param selectionArgs
         *            过滤条件参数
         * @return
         */
        public int removeSchedule() {
            if (mDBHelper == null) {
                return -1;
            }

            int result = mDBHelper.calendarDelete(
                    CalendarDatabaseHelper.TABLE_REMINDERS, null, null);

            if (result != -1) {
                return mDBHelper.calendarDelete(
                        CalendarDatabaseHelper.TABLE_EVENTS, null, null);
            } else {
                return -1;
            }

        }
    };

}
