/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendarProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.calendar;

import java.util.Arrays;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.util.Log;

public class CalendarProvider extends SQLiteContentProvider {

    private static final String TAG = "CalendarProvider";
    private static final int EVENTS = 1;
    private static final int EVENTS_ID = 2;
    private static final int REMINDERS = 3;
    private static final int REMINDERS_ID = 4;
    private static final int UPDATE_BROADCAST_MSG = 1;
    private static final long UPDATE_BROADCAST_TIMEOUT_MILLIS = DateUtils.SECOND_IN_MILLIS;
    private static final long SYNC_UPDATE_BROADCAST_TIMEOUT_MILLIS = 30 * DateUtils.SECOND_IN_MILLIS;
    private static final UriMatcher sUriMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(CalendarContrat.AUTHORITY, "events", EVENTS);
        sUriMatcher.addURI(CalendarContrat.AUTHORITY, "events/#", EVENTS_ID);

        sUriMatcher.addURI(CalendarContrat.AUTHORITY, "reminders", REMINDERS);
        sUriMatcher.addURI(CalendarContrat.AUTHORITY, "reminders/#",
                REMINDERS_ID);
    }

    private Context mContext;

    private final Handler mBroadcastHandler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            if (msg.what == UPDATE_BROADCAST_MSG) {
                doSendUpdateCalendar();

                // mContext.stopService(new Intent(mContext,
                // EmptyService.class));
            }
        };
    };

    private CalendarDatabaseHelper mDBHelper;

    protected static final String SQL_WHERE_ID = "_id=?";

    @Override
    public boolean onCreate() {
        super.onCreate();
        return initialize();
    }

    private boolean initialize() {
        mContext = getContext();
        mDBHelper = (CalendarDatabaseHelper) getDatabaseHelper();
        return true;
    }

    @Override
    protected SQLiteOpenHelper getDatabaseHelper(Context context) {
        return CalendarDatabaseHelper.getIntance(context);
    }

    @Override
    protected Uri insertInTransaction(Uri uri, ContentValues values,
            boolean callerIsSyncAdapter) {
        final int match = sUriMatcher.match(uri);
        long id = 0;
        switch (match) {
        case EVENTS:
            id = mDBHelper.calendarInsert(CalendarDatabaseHelper.TABLE_EVENTS,
                    values);

            if (id != -1) {
                sendUpdateCalendar(id, callerIsSyncAdapter);
            }
            break;
        case REMINDERS:
            id = mDBHelper.calendarInsert(
                    CalendarDatabaseHelper.TABLE_REMINDERS, values);

            if (id != -1) {
                sendUpdateCalendar(id, callerIsSyncAdapter);
            }
            break;

        case EVENTS_ID:
        case REMINDERS_ID:
            throw new UnsupportedOperationException(
                    "Calendar - Cannot insert into that URL: " + uri);
        }
        if (id < 0) {
            return null;
        }
        return ContentUris.withAppendedId(uri, id);
    }

    private void sendUpdateCalendar(long calendarsId,
            boolean callerIsSyncAdapter) {
        // Are there any pending broadcast requests?
        if (mBroadcastHandler.hasMessages(UPDATE_BROADCAST_MSG)) {
            // Delete any pending requests, before requeuing a fresh one
            mBroadcastHandler.removeMessages(UPDATE_BROADCAST_MSG);
        } else {
            // Because the handler does not guarantee message delivery in
            // the case that the provider is killed, we need to make sure
            // that the provider stays alive long enough to deliver the
            // notification. This empty service is sufficient to "wedge" the
            // process until we stop it here.
            // mContext.startService(new Intent(mContext, EmptyService.class));
        }
        // We use a much longer delay for sync-related updates, to prevent any
        // receivers from slowing down the sync
        long delay = callerIsSyncAdapter ? SYNC_UPDATE_BROADCAST_TIMEOUT_MILLIS
                : UPDATE_BROADCAST_TIMEOUT_MILLIS;
        // Despite the fact that we actually only ever use one message at a time
        // for now, it is really important to call obtainMessage() to get a
        // clean instance. This avoids potentially infinite loops resulting
        // adding the same instance to the message queue twice, since the
        // message queue implements its linked list using a field from Message.
        Message msg = mBroadcastHandler.obtainMessage(UPDATE_BROADCAST_MSG);
        mBroadcastHandler.sendMessageDelayed(msg, delay);
    }

    private void doSendUpdateCalendar() {
        Intent it = new Intent(Intent.ACTION_PROVIDER_CHANGED,
                CalendarContrat.CONTENT_URI);
        mContext.sendBroadcast(it, null);
    }

    @Override
    protected int updateInTransaction(Uri uri, ContentValues values,
            String selection, String[] selectionArgs,
            boolean callerIsSyncAdapter) {
        final int match = sUriMatcher.match(uri);
        Cursor calendars = null;
        String tableName;
        long id;
        try {
            if (match == EVENTS) {
                tableName = CalendarDatabaseHelper.TABLE_EVENTS;
                calendars = mDb.query(tableName, null, selection,
                        selectionArgs, null, null, null);
            } else if (match == EVENTS_ID) {
                tableName = CalendarDatabaseHelper.TABLE_EVENTS;
                id = ContentUris.parseId(uri);
                calendars = mDb.query(tableName, null, SQL_WHERE_ID,
                        new String[] { String.valueOf(id) }, null, null, null);
            } else if (match == REMINDERS) {
                tableName = CalendarDatabaseHelper.TABLE_REMINDERS;
                calendars = mDb.query(tableName, null, selection,
                        selectionArgs, null, null, null);
            } else if (match == REMINDERS_ID) {
                tableName = CalendarDatabaseHelper.TABLE_REMINDERS;
                id = ContentUris.parseId(uri);
                calendars = mDb.query(tableName, null, SQL_WHERE_ID,
                        new String[] { String.valueOf(id) }, null, null, null);
            } else {
                throw new IllegalArgumentException("Unknown Uri：" + uri);
            }

            if (calendars.getCount() == 0 || tableName == null) {
                Log.i(TAG,
                        "No events to update: uri=" + uri + " selection="
                                + selection + " selectionArgs="
                                + Arrays.toString(selectionArgs));
                return 0;
            }

            return handleUpdateEvents(tableName, calendars, values, selection,
                    selectionArgs, callerIsSyncAdapter);
        } finally {
            if (calendars != null) {
                calendars.close();
            }
        }
    }

    private int handleUpdateEvents(String tableName, Cursor cursor,
            ContentValues updateValues, String selection,
            String[] selectionArgs, boolean callerIsSyncAdapter) {
        if (cursor.getCount() > 1) {
            Log.d(TAG, "Performing update on " + cursor.getCount()
                    + " calendars");
        }

        // 更新操作的代码块
        mDBHelper.calendarUpdate(tableName, updateValues, selection,
                selectionArgs);

        return 0;
    }

    @Override
    protected int deleteInTransaction(Uri uri, String selection,
            String[] selectionArgs, boolean callerIsSyncAdapter) {
        // 删除操作的代码块
        final int match = sUriMatcher.match(uri);
        int result = -1;
        switch (match) {
        case EVENTS:
        case EVENTS_ID:
            result = mDBHelper.calendarDelete(
                    CalendarDatabaseHelper.TABLE_EVENTS, selection,
                    selectionArgs);
            break;

        case REMINDERS:
        case REMINDERS_ID:
            result = mDBHelper.calendarDelete(
                    CalendarDatabaseHelper.TABLE_REMINDERS, selection,
                    selectionArgs);
            break;
        default:
            break;
        }

        return result;
    }

    @Override
    protected void notifyChange(boolean syncToNetwork) {

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        Cursor cursor;
        mDb = mDBHelper.getReadableDatabase();

        long id;

        switch (sUriMatcher.match(uri)) {
        case EVENTS:
            sortOrder = (sortOrder == null ? CalendarContrat.Events.DEFAULT_SORT_ORDER
                    : sortOrder);
            cursor = mDb.query(CalendarDatabaseHelper.TABLE_EVENTS, projection,
                    selection, selectionArgs, null, null, sortOrder);
            break;
        case EVENTS_ID:
            sortOrder = (sortOrder == null ? CalendarContrat.Events.DEFAULT_SORT_ORDER
                    : sortOrder);
            id = ContentUris.parseId(uri);
            cursor = mDb.query(CalendarDatabaseHelper.TABLE_EVENTS, projection,
                    SQL_WHERE_ID, new String[] { String.valueOf(id) }, null,
                    null, sortOrder);
            break;

        case REMINDERS:
            cursor = mDb
                    .query(CalendarDatabaseHelper.TABLE_REMINDERS, projection,
                            selection, selectionArgs, null, null, sortOrder);
            break;
        case REMINDERS_ID:
            id = ContentUris.parseId(uri);
            cursor = mDb.query(CalendarDatabaseHelper.TABLE_REMINDERS,
                    projection, SQL_WHERE_ID,
                    new String[] { String.valueOf(id) }, null, null, sortOrder);
            break;
        default:
            throw new IllegalArgumentException("Unknown Uri：" + uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }
}
