/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendarProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.calendar;

import android.annotation.SuppressLint;
import android.net.Uri;

/**
 * 查询优化工具类
 * 
 * @author tZhang
 */
@SuppressLint("DefaultLocale")
public class QueryParameterUtils {

    public static boolean readBooleanQueryParameter(Uri uri, String name,
            boolean defaultValue) {
        final String flag = getQueryParameter(uri, name);
        return flag == null ? defaultValue : (!"false".equals(flag
                .toLowerCase()) && !"0".equals(flag.toLowerCase()));
    }

    // Duplicated from ContactsProvider2.
    /**
     * A fast re-implementation of {@link android.net.Uri#getQueryParameter}
     */
    public static String getQueryParameter(Uri uri, String parameter) {
        String query = uri.getEncodedQuery();
        if (query == null) {
            return null;
        }

        int queryLength = query.length();
        int parameterLength = parameter.length();

        String value;
        int index = 0;
        while (true) {
            index = query.indexOf(parameter, index);
            if (index == -1) {
                return null;
            }

            index += parameterLength;

            if (queryLength == index) {
                return null;
            }

            if (query.charAt(index) == '=') {
                index++;
                break;
            }
        }

        int ampIndex = query.indexOf('&', index);
        if (ampIndex == -1) {
            value = query.substring(index);
        } else {
            value = query.substring(index, ampIndex);
        }

        return Uri.decode(value);
    }
}
