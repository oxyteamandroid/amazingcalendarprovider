/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendarProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.calendar;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 日程合约类
 * 
 * @author tZhang
 * 
 */
public class CalendarContrat {
    public static final String AUTHORITY = "com.ingenic.calendar";
    public static final String CALLER_IS_SYNCADAPTER = "caller_is_syncadapter";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    private CalendarContrat() {
    }

    private interface EventColumns {
        /**
         * 标题
         */
        public final static String TITLE = "title";

        /**
         * 地点
         */
        public final static String EVENT_LOCATION = "event_location";

        /**
         * 日程描述
         */
        public final static String DESCRIPTION = "description";

        /**
         * 活动开始时间
         */
        public final static String DT_START = "dtstart";

        /**
         * 活动结束时间
         */
        public final static String DT_END = "dtend";

        /**
         * 时区
         */
        public final static String EVENT_TIMEZONE = "event_timezone";

        /**
         * 持续时间
         */
        public final static String DURATION = "duration";

        /**
         * 全天
         */
        public final static String ALL_DAY = "all_day";

        /**
         * 是否提醒
         */
        public final static String HAS_ALARM = "has_alarm";

        /**
         * 重复模式（无/每天/每月/每年/自定义）
         */
        public final static String RRULE = "rrule";
    }

    private interface ReminderColumns {
        /**
         * 日程ID
         */
        public final static String EVENT_ID = "event_id";

        /**
         * 提醒时间（提前10分钟）
         */
        public final static String MINUTES = "minutes";

        public final static String METHOD = "method";
    }

    public static final class Events implements BaseColumns, EventColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://"
                + AUTHORITY + "/events");
        public static final String DEFAULT_SORT_ORDER = DT_START + " ASC";
        public static final String[] PROVIDER_WRITABLE_COLUMNS = new String[] {
                TITLE, EVENT_LOCATION, DESCRIPTION, DT_START, DT_END,
                EVENT_TIMEZONE, DURATION, ALL_DAY, HAS_ALARM, RRULE };

        private Events() {
        };
    }

    public static final class Reminders implements BaseColumns, ReminderColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://"
                + AUTHORITY + "/reminders");
        public static final String[] PROVIDER_WRITABLE_COLUMNS = new String[] {
                EVENT_ID, MINUTES, METHOD };

        private Reminders() {
        };
    }
}
