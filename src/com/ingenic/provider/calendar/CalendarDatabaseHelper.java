/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingCalendarProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 日历日程数据库助手
 * 
 * @author tZhang
 */
public class CalendarDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "calendar.db";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE = "CREATE TABLE ";
    public static final String TABLE_EVENTS = "events";
    public static final String TABLE_REMINDERS = "reminders";
    private static CalendarDatabaseHelper sIntance;
    private SQLiteDatabase mDb;

    public static synchronized CalendarDatabaseHelper getIntance(Context context) {
        if (sIntance == null) {
            sIntance = new CalendarDatabaseHelper(context);
        }
        return sIntance;
    }

    private CalendarDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // 创建日程活动表
        db.execSQL(SQL_CREATE + TABLE_EVENTS + " ("
                + CalendarContrat.Events._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CalendarContrat.Events.TITLE + " TEXT, "
                + CalendarContrat.Events.EVENT_LOCATION + " TEXT, "
                + CalendarContrat.Events.DESCRIPTION + " TEXT, "
                + CalendarContrat.Events.DT_START + " INTEGER, "
                + CalendarContrat.Events.DT_END + " INTEGER, "
                + CalendarContrat.Events.EVENT_TIMEZONE + " TEXT, "
                + CalendarContrat.Events.DURATION + " TEXT, "
                + CalendarContrat.Events.ALL_DAY + " INTEGER, "
                + CalendarContrat.Events.HAS_ALARM + " INTEGER, "
                + CalendarContrat.Events.RRULE + " TEXT);");

        // 创建日程提醒表
        db.execSQL(SQL_CREATE + TABLE_REMINDERS + " ("
                + CalendarContrat.Reminders._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CalendarContrat.Reminders.EVENT_ID + " INTEGER, "
                + CalendarContrat.Reminders.MINUTES + " INTEGER, "
                + CalendarContrat.Reminders.METHOD + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 更新数据库版本
    }

    /**
     * Insert Calendar(插入日程信息)
     * 
     * @param values
     *            this map contains the initial column values for the row. The
     *            keys should be the column names and the values the column
     *            values(一个ContentValues对象，类似一个map.通过键值对的形式存储值。)
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long calendarInsert(String tableName, ContentValues values) {
        mDb = getWritableDatabase();
        Cursor cursor = mDb.query(tableName, null, "_id = ? ",
                new String[] { String.valueOf(values.getAsLong("_id")) }, null,
                null, null);
        if (cursor == null || cursor.getCount() == 0) {
            return mDb.insert(tableName, null, values);
        } else {
            return mDb.update(tableName, values, "_id = ? ",
                    new String[] { String.valueOf(values.getAsLong("_id")) });
        }
    }

    /**
     * Delete Calendar (删除日程信息)
     * 
     * @param whereClause
     *            the optional WHERE clause to apply when deleting. Passing null
     *            will delete all rows.(可选的where语句)
     * @param whereArgs
     *            (whereClause语句中表达式的？占位参数列表)
     * @return the number of rows affected if a whereClause is passed in, 0
     *         otherwise. To remove all rows and get a count pass "1" as the
     *         whereClause.(删除操作执行后受影响的行数)
     */
    public int calendarDelete(String tableName, String whereClause,
            String[] whereArgs) {
        mDb = getWritableDatabase();
        return mDb.delete(tableName, whereClause, whereArgs);
    }

    /**
     * Update Calendar (更新日程信息)
     * 
     * @param values
     *            a map from column names to new column values. null is a valid
     *            value that will be translated to
     *            NULL.(一个ContentValues对象，类似一个map.通过键值对的形式存储值。)
     * @param whereClause
     *            the optional WHERE clause to apply when updating. Passing null
     *            will update all rows.(可选的where语句)
     * @param whereArgs
     *            the group of args to deal with(whereClause语句中表达式的？占位参数列表)
     * @return the number of rows affected.(更新操作后受影响的行数)
     */
    public int calendarUpdate(String tableName, ContentValues values,
            String whereClause, String[] whereArgs) {
        mDb = getWritableDatabase();
        return mDb.update(tableName, values, whereClause, whereArgs);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        mDb = db;
    }
}
